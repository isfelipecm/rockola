/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.*;


/**
 *
 * @author camij
 */
public class Conexion {
    
    //Atributos    
    private static String DRIVER   ="com.mysql.jdbc.Driver";
    private static String USUARIO  ="root";
    private static String PASSWORD ="admin";
    private static String URL      ="jdbc:mysql://localhost:3306/rockola_3.0";
    
    //Métodos 
    public static Connection conectar() throws ClassNotFoundException, SQLException {
        
    Statement stmt;
    ResultSet rs;
    
        Class.forName(DRIVER);
        Connection con;
        con= DriverManager.getConnection(URL, USUARIO, PASSWORD);
        //JOptionPane.showMessageDialog(null, "Conexión exitosa", "Conexión", JOptionPane.INFORMATION_MESSAGE);
        return con;  
        
    }
    
        public static ResultSet ejecutarConsulta(String sql) throws ClassNotFoundException, SQLException{        
        Connection conex = conectar();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        if(sql.toUpperCase().startsWith("SELECT")){
            return statement.getResultSet();
        } else if (sql.toUpperCase().startsWith("UPDATE")){
            return null;
        } else if (sql.toUpperCase().startsWith("DELETE")){
            return null;
        } else if (sql.toUpperCase().startsWith("INSERT")){
            return null;
        }
        statement.close(); //Cerrar
        return null;
    }
   
    
}
