/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

public class Conexion_login {
    
    private int id_usuario;
    private int tipo;
    private String correo;
    private String pass;
    
    public Conexion_login() {
              
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD - Read
        String  sql = "SELECT tipo,correo,pass FROM usuarios WHERE id_usuario = "+this.getId_usuario()+";";
        ResultSet rs = Conexion.ejecutarConsulta(sql);// recibimos la tabla de la consulta que acabamos de hacer (tabla usuarios)
        while (rs.next()) // Recorremos la tabla
        {
          this.setTipo(rs.getInt("id_usuario"));
          this.setCorreo(rs.getString("correo"));
          this.setPass(rs.getString("pass"));
        }
        
        if (this.getCorreo() != ""){
            return true;
        } else {
            return false;
        }
        
    }

    
}
