
package Controlador;
import Modelo.Conexion;
import Modelo.Conexion_login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Canciones {
   
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Conexion cn = new Conexion();
    
    public Conexion_login log(String correo, String pass) throws ClassNotFoundException{
        
        Conexion_login l = new Conexion_login();
        String sql = "SELECT * FROM usuarios WHERE correo = ? AND pass= ?";
        try {
            con = cn.conectar();
            ps = con.prepareStatement(sql);
            ps.setString(1, correo);
            ps.setString(2, pass);
            rs=ps.executeQuery();
            if(rs.next()){
               
                l.setId_usuario(rs.getInt("id_usuario"));
                l.setTipo(rs.getInt("tipo"));
                l.setCorreo(rs.getString("correo"));
                l.setPass(rs.getString("pass"));
                
            }
                    
        } catch (SQLException e) {
            
            System.out.println(e.toString());
        }
        return l;
    }

    // Atributos
    private int id_cancion;
    private String nombre_cancion;
    private int anio;
    private String formato;
    private String ruta;
    private String autor;
    private String genero;

    // Constructor
    public Canciones(){}

    public Canciones(int id_cancion, String nombre_cancion, int anio, String formato, String ruta, String autor, String genero) {
        this.id_cancion = id_cancion;
        this.nombre_cancion = nombre_cancion;
        this.anio = anio;
        this.formato = formato;
        this.ruta = ruta;
        this.autor = autor;
        this.genero = genero;        
    }

    public int getId() {
        return id_cancion;
    }

    public void setId(int id_cancion) {
        this.id_cancion = id_cancion;
    }

    public String getNombre() {
        return nombre_cancion;
    }

    public void setNombre(String nombre_cancion) {
        this.nombre_cancion = nombre_cancion;
    } 

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
       
    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
    
    public String getGenero() {
        return genero;
    }
    
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    // Métodos - Funciones

    // CRUD => Create Read Update Delete
    
    // Guardar
    public boolean guardar(){
        //CRUD - Create
        String  sql = "INSERT INTO canciones(id_cancion, nombre_cancion, anio, formato, ruta, autor, genero) VALUES (?,?,?,?,?,?,?)";
        
        try {
            con = cn.conectar();
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.setString(2, getNombre());
            ps.setInt(3, getAnio());
            ps.setString(4, getFormato());
            ps.setString(5, getRuta());
            ps.setString(6, getAutor());
            ps.setString(7, getGenero());
            ps.execute();
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return  false;
        }
       
    }
    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD - Read
        String  sql = "SELECT id_cancion, nombre_cancion, anio, formato, ruta, autor, genero FROM canciones";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while(rs.next()) // Mientras se traiga todos los datos del id_cancion => Cursor.
        {
          this.setId(rs.getInt("id_cancion")); 
          this.setNombre(rs.getString("nombre_cancion"));
          this.setAnio(rs.getInt("anio")); 
          this.setFormato(rs.getString("formato"));
          this.setRuta(rs.getString("ruta"));
          this.setAutor(rs.getString("autor"));
          this.setGenero(rs.getString("genero"));
        }
        
        if (this.getId() != 0){
            return true;
        } else {
            return false;
        }
    }

    
    // Actualizar
    
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD - Update
        String sql = "UPDATE canciones SET nombre_cancion = "+getNombre()+", anio = "+getAnio()+", formato = "+getFormato()+", ruta = "+getRuta()+",autor = "+getAutor()+", genero = "+getGenero()+"WHERE id_cancion ="+getId()+";";
        Conexion.ejecutarConsulta(sql);
    }
    
    // Borrar
   
    public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD - Delete
        String sql = "DELETE FROM canciones WHERE id_canciones ="+getId()+";";
        Conexion.ejecutarConsulta(sql);
    }
    
    public List ListarCanciones(){
        List<Canciones> ListaCl = new ArrayList(); 
        String sql = "SELECT * FROM canciones";
        try {
            con = cn.conectar();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                Canciones cl = new Canciones();
                cl.setId(rs.getInt("id_cancion")); 
                cl.setNombre(rs.getString("nombre_cancion"));
                cl.setAnio(rs.getInt("anio")); 
                cl.setAutor(rs.getString("autor"));
                cl.setGenero(rs.getString("genero"));
                cl.setRuta(rs.getString("ruta"));
                cl.setFormato(rs.getString("formato"));
                ListaCl.add(cl);
                
            }
        } catch (Exception e) {
            
            System.out.println(e.toString());
        }
        return ListaCl;
    }
    
    public boolean eliminarCancion(int id){
        String sql= "DELETE FROM canciones WHERE id_cancion = ?";
        try {
            ps= con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false; 
            
        }finally{
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
    }
    
    public boolean ModificarCanciones(){
        String sql ="UPDATE canciones SET nombre_cancion=?, anio=?, formato=?, ruta=?, autor=?, genero=? WHERE id_cancion=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getNombre());
            ps.setInt(2, getAnio());
            ps.setString(3, getFormato());
            ps.setString(4, getRuta());
            ps.setString(5, getAutor());
            ps.setString(6, getGenero());
            ps.setInt(7, getId());
            ps.execute();
            return true;
        } catch (Exception e) {
            
            System.out.println(e.toString());
            return false;
        }finally{
            try {
                con.close();
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
    }
}
